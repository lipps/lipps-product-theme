<?php
/**
 * Short code collection
 *
 * @package LIPPS Product
 */

function lipps_recent_news_posts() {
	$posts = get_posts( array(
			'post_type' => 'news',
			'post_status' => 'publish',
			'posts_per_page' => 4
		)
	);
	$html = '';
	foreach ( $posts as $p ) {
		$image_url = get_the_post_thumbnail_url( $p->ID, 'medium' );
		if ( ! $image_url ) {
			$image_url = get_template_directory_uri() . '/images/default.jpg';
		}

		$html .= '<div class="lipps-recent-news-article">';
		$html .= '<a href="'.get_the_permalink( $p->ID ).'" class="lipps-news-link"></a>';
		$html .= '<div class="lipps-news-image-container"><img src="'.esc_url( $image_url ).'" alt="" class="lipps-news-image"></div>';
		$html .= '<div class="lipps-news-headline"><p class="lipps-news-headline title">'.$p->post_title.'</p></div>';
		$html .= '<div class="lipps-news-date-container"><p class="lipps-news-date">'.get_the_time( 'Y.m.d', $p->ID ).'</p></div>';
		$html .= '</div>';
	}

	return sprintf('
		<div class="lipps-recent-news-container alignfull">
		  <div class="lipps-recent-news-articles">
		    <div class=" lipps-news-article-list recent-post">
		      %1$s
		    </div>
		  </div>
          <div class="lipps-news-return-link-container">
            <a href="'. get_post_type_archive_link( 'news'  ).'" class="lipps-news-return-link right">ARCHIVE ></a>
          </div>
		</div>', $html
	);
}
add_shortcode('recent-news-posts', 'lipps_recent_news_posts');

