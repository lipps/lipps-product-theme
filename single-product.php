<?php
/**
 * The template for displaying posts for product
 *
 * @package LIPPS Product
 */
?>

<?php get_header(); ?>
<main>
    <?php while( have_posts() ) : the_post(); ?>
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <?php
          the_content();
          ?>
      </article>
    <?php endwhile; ?>
    <?php get_template_part('template-parts/product', 'menu') ?>
</main>

<?php get_footer(); ?>
