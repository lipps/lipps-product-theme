<?php

/**
 * Template Name: News archive
 *
 * @package LIPPS Product
 */

?>

<?php get_header(); ?>
<main>
  <div class="lipps-news">
    <h2 class="main-content-heading">NEWS</h2>
    <div class="lipps-news-container">
      <div class="lipps-news-articles">
        <div class="lipps-news-article-list">
			<?php
			$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;

			$args = array(
				'post_type'      => 'news',
				'posts_per_page' => get_option( 'posts_per_page' ),
				'paged'          => $paged
			);

			$local_query = new WP_Query( $args );

			// ページャーの表示
			if ( function_exists( 'lipps_product_pagination' ) ) {
				lipps_product_pagination( $local_query->max_num_pages, $paged );
			}

			$posts = get_posts( $args );
			foreach ( $posts as $news ) :
				$thumbnail_url = get_the_post_thumbnail_url( $news->ID, 'medium' );
				if ( ! $thumbnail_url ) {
					$thumbnail_url = get_template_directory_uri() . '/images/default.jpg';
				} ?>

        <div class="lipps-news-article">
          <a href="<?php echo get_the_permalink( $news->ID ); ?>" class="lipps-news-link"></a>
          <div class="lipps-news-image-container">
            <img src="<?php echo esc_url( $thumbnail_url ); ?>" alt="" class="lipps-news-image">
          </div>
          <div class="lipps-news-headline">
            <p class="lipps-news-headline title"><?php echo $news->post_title; ?></p>
          </div>
          <div class="lipps-news-date-container">
            <p class="lipps-news-date"><?php echo get_the_time( 'Y.m.d', $news->ID ); ?></p>
          </div>
        </div>
			<?php endforeach;

			// ページャーの表示
      if ( function_exists( 'lipps_product_pagination' ) ) {
          lipps_product_pagination( $local_query->max_num_pages, $paged );
      }
      ?>
        </div>
      </div>
    </div>
  </div>
  <?php get_template_part( 'template-parts/product', 'menu' ) ?>
</main>
<?php get_footer(); ?>
