<?php
/**
 * The header for LIPPS theme
 *
 * @package LIPPS Product
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> >

<html>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Work+Sans" rel="stylesheet">

  <!--[if lt IE 9]>
  <script src="<?php echo esc_url( get_theme_file_uri( '/js/html5shiv.min.js' ) ); ?>"></script>
  <![endif]-->

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header id="top" data-collapse="small" data-animation="over-left" data-duration="400" data-no-scroll="1" class="navbar w-nav">
  <div class="w-container">
    <div class="nav-wrapper">
      <div class="brand">
        <div class="brand-logo">
	        <?php get_template_part('template-parts/header', 'logo')?>
        </div>
      </div>
      <?php get_template_part('template-parts/navigation', 'main')?>
    </div>
  </div>
  <div class="headerborder-line"></div>
</header>

