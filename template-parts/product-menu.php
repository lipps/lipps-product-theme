<?php
/**
 * For displaying product menu
 *
 * @package LIPPS Product
 */
?>

<aside class="aside">
  <div class="w-container">
    <h2 class="main-content-heading">
      Product<br>
      Menu
    </h2>
    <?php

    // for phone size display
    $indicator_url = get_template_directory_uri() . '/images/switching_button.png';

    $num_category     = wp_count_terms( 'category' );
    $categories       = get_categories();

    foreach ( $categories as $category ) :
    if ( empty( $category ) ) { continue; }
    if ( $category->cat_name === 'Uncategorized' ) { continue; }
    ?>
    <div class="product-menu-accordion">
      <div data-delay="0" class="dropdown w-dropdown">
        <div class="dropdown-toggle w-dropdown-toggle">
          <div> <?php echo $category->cat_name; ?> </div>
          <img src="<?php echo esc_url($indicator_url); ?>" alt="" class="image-2" width="30">
        </div>
        <nav class="dropdown-list w-dropdown-list">
	        <?php lipps_get_product_menu_items( $category ); ?>
        </nav>
      </div>
    </div>
    <?php endforeach;

    // for large size display
    ?>
    <div class="product-menu">
      <?php
      $category_counter = 0;

      foreach ( $categories as $category ) :
      if ( empty( $category ) ) { continue; }
      if ( $category->cat_name === 'Uncategorized' ) { continue; }
      ?>

      <div class="product-menu-nav">
        <?php $border_line = ($category_counter % 3 === 0) ? 'no-border' : '' ?>
        <div class="product-menu-contents <?php echo $border_line; ?>">
          <h3 class="product-menu-heading"> <?php echo $category->cat_name; ?> </h3>
	        <?php lipps_get_product_menu_items( $category ); ?>
        </div>
      </div>
      <?php ++$category_counter; ?>
      <?php endforeach; ?>
    </div>
    <div class="inquiry">
      <div class="rich-text-block w-richtext">
        <p>
          商品のお問い合わせについて</p>
        <p>
          商品の品質管理には万全を期しておりますが、万が一、商品に不備があった場合には、<br>
          お手数ですが下記にあるご連絡先までご連絡ください。<br>
          不備があった商品の実物の確認が取れない場合には交換、返品をお受けできない場合がございます。</p>
        <p>
          TEL : 03-5468-0189<br>
        </p>
      </div>
    </div>
  </div>
</aside>

<?php
function lipps_get_product_menu_items( $category ) {

  $args = array(
      'numberposts' => -1,
      'post_type' => 'product',
      'category' => $category->term_id,
  );

  echo '<ul class="product-menu-items">';
  $posts = get_posts($args);
  foreach( $posts as $p ) {
	  echo '<li class="product-menu-item">';
    echo '<a href="' . esc_url( get_the_permalink( $p->ID ) ) . '" class="product-menu-item-link">' . $p->post_title . '</a>';
    echo '</li>';
  }
  echo '</ul>';
}
?>
