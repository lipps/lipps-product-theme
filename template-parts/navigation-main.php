<?php
/**
 * For displaying global navigation
 *
 * @package LIPPS Product
 */
?>

<nav role="navigation" class="nav-menu w-nav-menu">
  <div class="nav-close-wrapper">
    <div class="menu-button w-nav-button">
      <img src="<?php echo esc_url( get_template_directory_uri() . '/images/close-menu.png' ); ?>" alt="" width="18">
    </div>
  </div>

  <?php
  $locations = get_nav_menu_locations();
  $menu = wp_get_nav_menu_object( $locations['main-menu'] );
  $items = wp_get_nav_menu_items( $menu->term_id );

  foreach ( $items as $item ) : ?>
    <a href="<?php echo esc_url( $item->url ); ?>" class="nav-menu-item w-nav-link" style="max-width: 940px;"><?php echo $item->title; ?></a>
  <?php endforeach; ?>

  <div class="for-mobile">
    <div><a href="<?php echo esc_url( home_url() ); ?>" class="link-2 w--current">トップへ</a></div>
    <div class="nav-sns">
      <ul class="list-3 w-list-unstyled">
        <?php
        $locations = get_nav_menu_locations();
        $menu = wp_get_nav_menu_object( $locations['menu-media'] );
        $items = wp_get_nav_menu_items( $menu->term_id );
	      foreach ( $items as $item ) : ?>
          <li><a href="<?php echo esc_url( $item->url ); ?>" target="_blank" class="link-icon w-inline-block <?php echo $item->post_name; ?>"></a></li>
 	       <?php endforeach; ?>
      </ul>
    </div>
  </div>
</nav>
<div class="menu-button w-nav-button">
  <div class="nav-lines">
    <div class="nav-line"></div>
    <div class="nav-line"></div>
  </div>
</div>



